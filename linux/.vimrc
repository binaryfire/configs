call plug#begin('~/.vim/plugged')
" colorscheme gotham
Plug 'whatyouhide/vim-gotham'
" colorscheme iceberg
Plug 'cocopon/iceberg.vim'
" colorscheme solarized
Plug 'lifepillar/vim-solarized8'
" Language Server plugins
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
call plug#end()

" colorscheme habamax
" colorscheme gotham
" colorscheme iceberg
" colorscheme murphy
colorscheme solarized8

set background=dark

set autoindent
set expandtab
set ruler
set number
set hlsearch
set nocompatible
set mouse=r

let mapleader="a"

nnoremap <leader>rel :source ~/.vimrc<ENTER>

nnoremap <leader>wd   :LspDefinition<ENTER>
nnoremap <leader>wr   :LspReferences<ENTER>

nnoremap <leader>gca :!git commit --amend<ENTER>
nnoremap <leader>gdi :!git diff %<ENTER>
nnoremap <leader>glo :!git log<ENTER>
nnoremap <leader>gst :!git status<ENTER>
nnoremap <leader>gpo :!git push origin HEAD:refs/for/master<ENTER>
nnoremap <leader>gre :!git switch master; git pull; git switch -; git rebase master<ENTER>

" These should be moved into an autocmd performed on buffer save action.
" Format Go files.
nnoremap <leader>gof :!go fmt %<ENTER>
" A tool to format C/C++/Java/JavaScript/JSON/Objective-C/Protobuf/C# code.
nnoremap <leader>cpf  :!clang-format -i %<ENTER>

" The clang-format style
autocmd FileType cpp setlocal shiftwidth=2 softtabstop=2

source ~/.vimrc_other
