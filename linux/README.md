Run the command as following to preserve the environment:
	
    * bitbucket: get link to raw text `setup.sh`. Replace it below
    * run: `curl <link> -o setup.sh`
    * run: `chmod a+x setup.sh`
    * run: `./setup.sh`

The command will download go, config git and will then execute 'setup.go' which installs all the other needed packages.

TODO: Pip gives an error that running as root may cause permission issues. This should be fixed in the future.
