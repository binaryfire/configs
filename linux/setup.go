package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"strings"
)

var USER_FLAG string

type PackageId int

const (
	Bazel = iota
	VimPlug
	Pip
	Pyls
	Python
	Configs
	Go
	Gopls
	Clangd
	ClangFormat
	Flatpak
	Firefox
)

type InstallFunc func(io.Writer, io.Writer) error

type Package struct {
	id      PackageId
	install InstallFunc
	deps    []PackageId
}

func NewPackage(id PackageId, install InstallFunc, dep ...PackageId) Package {
	return Package{
		id:      id,
		install: install,
		deps:    dep,
	}
}

func (p Package) String() string {
	switch p.id {
	case Bazel:
		return fmt.Sprintf("{Package Bazel}")
	case VimPlug:
		return fmt.Sprintf("{Package VimPlug}")
	case Python:
		return fmt.Sprintf("{Package Python}")
	case Pip:
		return fmt.Sprintf("{Package Pip}")
	case Pyls:
		return fmt.Sprintf("{Package Pyls}")
	case Go:
		return fmt.Sprintf("{Package Go}")
	case Gopls:
		return fmt.Sprintf("{Package Gopls}")
	case Configs:
		return fmt.Sprintf("{Package Configs}")
	case Clangd:
		return fmt.Sprintf("{Package Clangd - https://clangd.llvm.org/}")
	case ClangFormat:
		return fmt.Sprintf("{Package ClangFormat - https://clang.llvm.org/docs/ClangFormat.html}")
	case Flatpak:
		return fmt.Sprintf("{Package Flatpak - https://flatpak.org/setup/Chrome%20OS}")
	case Firefox:
		return fmt.Sprintf("{Package Firefox - https://support.mozilla.org/en-US/kb/run-firefox-chromeos}")
	}

	panic(fmt.Sprintf("{Unknow package}"))
}

func runCmd(stdout io.Writer, stderr io.Writer, cmd string, arg ...string) error {
	execCmd := exec.Command(cmd, arg...)
	execCmd.Stdout = stdout
	execCmd.Stderr = stderr
	if err := execCmd.Run(); err != nil {
		return err
	}

	return nil
}

func httpGet(url string, path string) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	bytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return err
	}
	defer f.Close()

	if _, err := f.Write(bytes); err != nil {
		return err
	}

	return nil
}

func createFileWithContent(content io.Reader, path string) error {
	if err := os.Remove(path); err != nil {
		log.Printf("Failed removing file %s: %v", path, err)
	}
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE, 0777)
	if err != nil {
		return err
	}
	defer f.Close()

	write, err := io.ReadAll(content)
	if err != nil {
		return err
	}
	if _, err := f.Write([]byte(write)); err != nil {
		return err
	}

	return nil
}

func installPython(stdout io.Writer, stderr io.Writer) error {
	if err := runCmd(stdout, stderr, "apt-get", "-y", "install", "python3"); err != nil {
		return err
	}
	return nil
}

func installPip(stdout io.Writer, stderr io.Writer) error {
	if err := runCmd(stdout, stderr, "apt-get", "-y", "install", "python3-pip"); err != nil {
		return err
	}
	return nil
}

func installPyls(stdout io.Writer, stderr io.Writer) error {
	if err := runCmd(stdout, stderr, "pip", "install", "python-language-server"); err != nil {
		return err
	}
	return nil
}

func installGopls(stdout io.Writer, stderr io.Writer) error {
	if err := runCmd(stdout, stderr, "apt-get", "-y", "install", "gopls"); err != nil {
		return err
	}
	return nil
}

func installGo(stdout io.Writer, stderr io.Writer) error {
	if _, err := os.Stat("/usr/local/go/bin"); os.IsNotExist(err) {
		return fmt.Errorf("Go should be installed by setup.sh, err=%w", err)
	}
	return nil
}

func installClangd(stdout io.Writer, stderr io.Writer) error {
	if err := runCmd(stdout, stderr, "apt-get", "-y", "install", "clangd"); err != nil {
		return err
	}
	return nil
}

func installClangFormat(stdout io.Writer, stderr io.Writer) error {
	if err := runCmd(stdout, stderr, "apt-get", "-y", "install", "clang-format"); err != nil {
		return err
	}
	return nil
}

func installBazel(stdout io.Writer, stderr io.Writer) error {
	if err := runCmd(stdout, stderr, "apt-get", "-y", "install", "gnupg"); err != nil {
		return err
	}
	if err := httpGet("https://bazel.build/bazel-release.pub.gpg", "/tmp/bazel-release.pub.gpg"); err != nil {
		return err
	}
	if err := os.Remove("/usr/share/keyrings/bazel-archive-keyring.gpg"); err != nil {
		log.Printf("Failed removing file: %v", err)
	}
	if err := runCmd(stdout, stderr, "gpg", "--output", "/usr/share/keyrings/bazel-archive-keyring.gpg", "--dearmor", "/tmp/bazel-release.pub.gpg"); err != nil {
		return err
	}
	if err := createFileWithContent(
		strings.NewReader("deb [arch=amd64 signed-by=/usr/share/keyrings/bazel-archive-keyring.gpg] https://storage.googleapis.com/bazel-apt stable jdk1.8"),
		"/etc/apt/sources.list.d/bazel.list"); err != nil {
		return err
	}
	if err := runCmd(stdout, stderr, "apt-get", "update"); err != nil {
		return err
	}
	if err := runCmd(stdout, stderr, "apt-get", "install", "bazel", "-y"); err != nil {
		return err
	}

	return nil
}

func installVimPlug(stdout io.Writer, stderr io.Writer) error {
	homeDir := path.Join("/home/", USER_FLAG)
	log.Printf("Creating directory %v", path.Join(homeDir, ".vim/autoload"))
	if err := os.MkdirAll(path.Join(homeDir, ".vim/autoload"), 0777); err != nil {
		return err
	}
	if err := httpGet(
		"https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim",
		path.Join(homeDir, ".vim/autoload/plug.vim")); err != nil {
		return err
	}
	// change owner from root to user
	if err := runCmd(stdout, stderr, "chown", USER_FLAG, path.Join(homeDir, ".vim")); err != nil {
		return err
	}
	stdout.Write([]byte("VimPlug is now installed. After vim configs are installed, execute in vim ':PlugInstall' to install all plugins."))
	return nil
}

func installConfigs(stdout io.Writer, stderr io.Writer) error {
	homeDir := path.Join("/home/", USER_FLAG)
	f1, err := os.OpenFile(".vimrc", os.O_RDONLY, 0777)
	if err != nil {
		return err
	}
	defer f1.Close()

	f2, err := os.OpenFile(".vimrc_other", os.O_RDONLY, 0777)
	if err != nil {
		return err
	}
	defer f2.Close()

	if err := createFileWithContent(f1, path.Join(homeDir, ".vimrc")); err != nil {
		return err
	}
	if err := createFileWithContent(f2, path.Join(homeDir, ".vimrc_other")); err != nil {
		return err
	}

	// change owner from root to user
	if err := runCmd(stdout, stderr, "chown", USER_FLAG, path.Join(homeDir, ".vimrc"), path.Join(homeDir, ".vimrc_other")); err != nil {
		return err
	}
	stdout.Write([]byte(".vimrc and .vimrc_other were copied to " + homeDir))

	return nil
}

func installFlatpak(stdout io.Writer, stderr io.Writer) error {
	if err := runCmd(stdout, stderr, "apt-get", "-y", "install", "flatpak"); err != nil {
		return err
	}
	if err := runCmd(stdout, stderr, "flatpak", "--system", "remote-add", "--if-not-exists", "flathub", "https://dl.flathub.org/repo/flathub.flatpakrepo"); err != nil {
		return err
	}
	return nil
}

func installFirefox(stdout io.Writer, stderr io.Writer) error {
	if err := runCmd(stdout, stderr, "flatpak", "install", "-y", "--system", "https://dl.flathub.org/repo/appstream/org.mozilla.firefox.flatpakref"); err != nil {
		return err
	}
	return nil
}

func install(packages []Package) {
	var installed map[PackageId]bool = make(map[PackageId]bool)

	for len(installed) != len(packages) {
		for _, p := range packages {
			if _, ok := installed[p.id]; ok {
				// Package is already installed
				continue
			}

			missingDep := false
			for _, dep := range p.deps {
				if _, ok := installed[dep]; !ok {
					missingDep = true
					break
				}
			}
			if missingDep {
				// Package not ready yet to be installed
				continue
			}

			log.Printf("\n--------------------Installing %v--------------------\n", p)
			if err := p.install(os.Stdout, os.Stderr); err != nil {
				log.Printf("Failed installing %v: %v", p, err)
				return
			}

			installed[p.id] = true
		}
	}
}

func main() {
	flag.StringVar(&USER_FLAG, "user", "user", "The user that the configs should be setup for.")
	flag.Parse()

	packages := []Package{
		NewPackage(Flatpak, installFlatpak),
		NewPackage(Firefox, installFirefox, Flatpak),
		NewPackage(Clangd, installClangd),
		NewPackage(ClangFormat, installClangFormat),
		NewPackage(Pyls, installPyls, Pip),
		NewPackage(Go, installGo),
		NewPackage(Gopls, installGopls),
		NewPackage(Pip, installPip, Python),
		NewPackage(Python, installPython),
		NewPackage(Bazel, installBazel),
		NewPackage(VimPlug, installVimPlug),
		NewPackage(Configs, installConfigs, VimPlug, Pyls, Gopls),
	}

	// can go into an infinite loop if the dependencies are not satisfiable
	install(packages)
}
