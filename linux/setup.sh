#!/bin/bash

# https://go.dev/doc/install
wget -O go.tar.gz https://go.dev/dl/go1.20.1.linux-amd64.tar.gz
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go.tar.gz

touch $HOME/.profile
if ! grep -q "export PATH=\$PATH:/usr/local/go/bin" $HOME/.profile; then
	echo "export PATH=\$PATH:/usr/local/go/bin" >> $HOME/.profile
fi
export PATH=$PATH:/usr/local/go/bin

echo "Installed Golang"
go version

sudo apt-get update
sudo apt-get install -y git

# https://www.atlassian.com/git/tutorials/git-ssh
echo "Generating SSH key"
ssh-keygen -t rsa -b 4096 -C "florin.pogocsan@gmail.com"
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_rsa

echo "Add the following key to bitbucket (Settings -> Personal Bitbucket settings -> SSH keys -> Add key)"
cat ~/.ssh/id_rsa.pub

read -p "Press enter when done"

git config --global user.email "florin.pogocsan@gmail.com"
git config --global user.name "Florin Pogocsan"
git config --global core.editor "vim"

mkdir -p ~/src/configs
git clone git@bitbucket.org:binaryfire/configs.git ~/src/configs

pushd ~/src/configs/linux
go build setup.go
sudo ./setup -user=$USER
popd
