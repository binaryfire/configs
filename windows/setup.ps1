Write-Host "Install chocolatey"

if (!(Get-Command choco)) {
    Write-Host "---- Already installed"
    Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))
}

Write-Host "Install packages through chocolatey."
choco install vim
choco install ghc --version 9.0.1
choco install haskell-language-server

cp $PSScriptRoot\_vimrc $HOME

mkdir -p $HOME\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState
cp $PSScriptRoot\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json $HOME\AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json

# Disable navigation to unwanted websites
cp $PSScriptRoot\hosts C:\Windows\System32\drivers\etc\hosts
C:\Windows\System32\ipconfig /flushdns

# Disable useless features
# Takes screenshots every few seconds and analyzes past activity
Dism /Online /Disable-Feature /Featurename:Recall
